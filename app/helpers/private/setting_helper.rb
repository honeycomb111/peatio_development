module Private::SettingHelper
	
	def display_country country=nil
    @countries = ISO3166::Country.all.sort
    options_for_select(@countries, country)
  end

  SEQUENCE_HASH = {
    'email_verified':     1,
    'phone_verified':     2,
    'document_verified':  3,
    'identity_verified':  4
  }

  def is_verified? level_check
    SEQUENCE_HASH[current_user.level.to_sym] >= level_check
  end

  def icons_tags_str  level_match_status
    level_match_status ?
     "<i class='fa fa-check-circle-o' aria-hidden='true' style='color: forestgreen;font-size: 25px;vertical-align: sub;'></i><span style='color: forestgreen;font-size: 14px;vertical-align: baseline;'>Verified</span>" :
      "<i class='fa fa-times-circle-o' aria-hidden='true' style='color: palevioletred;font-size: 25px;vertical-align: sub;'></i><span style='color: palevioletred;font-size: 14px;vertical-align: baseline;'>Unverified</span>"
  end
end  