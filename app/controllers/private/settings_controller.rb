module Private
  class SettingsController < BaseController
    before_action :set_previous_details, except: [:index, :change_password, :verify_contact, :referrals_process]
    before_action :set_member_and_status, except: [:index]

    def index
    end

    def change_avatar
      if @member.update(avatar: params[:avatar])
        redirect_to settings_path,alert: "Image updted successfully"
      else
        redirect_to settings_path,alert: "Please try again"
      end  
    end

    def change_password
      if request.put?
        begin
          abc = RestClient.put(
            "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/accounts/password",
            params.require(:member).permit!.merge({access_token: current_user.auth('barong').token})
          )
          @message = "Password Changed!"
          @status = true
        rescue => e
          @message = e.message
        end
      end
    end

    def verify_contact
      if request.put?
        begin
          phone_params = params.require(:member).permit(:country_code, :phone_number, :verification_code)
          phone_params[:phone_number] = phone_params.delete(:country_code)+phone_params.delete(:phone_number)
          abc = RestClient.post(
            "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/phones/verify",
            phone_params.merge({access_token: current_user.auth('barong').token})
          )
          @message = JSON.parse(abc.body)['message']
          @status = true
        rescue => e
          @message = e.message
        end
      elsif request.post?
        begin
          params.require(:phone_number) rescue (raise 'Phone Number is blank!')
          abc = RestClient.post(
            "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/phones",
            {phone_number: params.require(:phone_number)}.merge({access_token: current_user.auth('barong').token})
          )
          render json: {status: true, message: JSON.parse(abc.body)['message']}
        rescue => e
          render json: {status: false, message: e.message}
        end
      end
    end

    def update_profile
      if request.post?
        begin
          abc = RestClient.post(
            "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/profiles",
            profile_params.merge({access_token: current_user.auth('barong').token})
          )
          @status = true
          @message = "Profile Updated!"
        rescue => e
          @message = e.message
        end
      end
    end

    def upload_documents
      if request.post?
        begin
          abc = RestClient.post(
            "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/documents",
            upload_document_params.merge({access_token: current_user.auth('barong').token})
          )
          @status = true
          @message = "Document Uploaded!"
        rescue => e
          @message = e.message
        end
      end
    end

    def referrals_process
    end

    private
    def set_member_and_status
      @member = current_user
      @status = false
      @referred_members = current_user.referred_members.includes(:ref_member) if action_name=="referrals_process"
      render(partial: params[:action]) if request.get?
    end

    def set_previous_details
      if request.get?
        begin
          @current_details = RestClient.get(
            "#{previous_data_api}?access_token=#{current_user.auth('barong').token}"
          )
          @current_details = JSON.parse(@current_details.body) {}
        rescue 
           @current_details = {}
        end
      end
    end

    def previous_data_api
      {
        update_profile: "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/profiles/me",
        upload_documents: "#{ENV.fetch('BARONG_DOMAIN')}/api/v1/documents"
      }[params[:action].to_sym]
    end

    def profile_params
      member_params = params.require(:member)
      {
        first_name:   member_params.require(:first_name),
        last_name:    member_params.require(:last_name),
        dob:          member_params.require(:dob),
        address:      member_params.require(:address),
        postcode:     member_params.require(:postcode),
        city:         member_params.require(:city),
        country:      member_params.require(:country)
      }
    end

    def upload_document_params
      document_params = params.require(:document)
      {
        doc_expire:   document_params.require(:doc_expire),
        doc_type:     document_params.require(:doc_type),
        doc_number:   document_params.require(:doc_number),
        upload:       document_params.require(:upload)
      }
    end
  end
end

